<?php
Flight::route('/', function(){
	session_start();

	require CONNECTION;
	require VALIDATE;

	include LISTING_TASK;

	$data = array(
        'base_url' => BASE_URL,
        'site' => SITE,
		'menu' => 'Home',
		'title' => 'Site X - Home',
		'tasks' => $listing,
    );

	Flight::view()->display('vSite_1_Home.twig', $data);
});

Flight::route('POST /register', function(){

    require CONNECTION;
    require VALIDATE;

	$task = $_POST['task'];

    include REGISTER_TASK;

    echo "success";

});

Flight::route('POST /update', function(){

    require CONNECTION;
    require VALIDATE;

	$id = $_POST['id'];
	$task = $_POST['task'];

	include UPDATE_TASK;

    echo "success";
});

Flight::route('POST /delete', function(){

    require CONNECTION;
    require VALIDATE;

	$id = $_POST['id'];

    include DELETE_TASK;

    echo "success";
});

Flight::route('POST /update-status', function(){

    require CONNECTION;
    require VALIDATE;

	$id = $_POST['id'];
	if ($_POST['status'] == 1) {
        $status = 0;
    }

    if ($_POST['status'] == 0) {
        $status = 1;
    }

    include UPDATE_STATUS_TASK;

    echo "success";
});