<?php

Flight::map('notFound', function(){
    session_start();
	require CONNECTION;

	$data = array(
		'base_url' => BASE_URL,
		'site' => SITE,
		'titulo' => "Site X - Página não encontrada",
		'menu' => "404"
	);
	//$query = array_merge($data, $lang);

	Flight::view()->display('vError_1_404.twig', $data);
});