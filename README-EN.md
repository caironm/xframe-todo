[Copy and paste the entire contents of this document into dillinger.io to view] (http://dillinger.io/)

# xFRAME - Quick Start Guide

### WHAT IS
It is not a framework, but a set of modular configurations to behave as one, without leaving the documentation and organization aside. This product does not reinvent the wheel but it certainly will make you feel that you have developed your own wheel. The biggest benefit is the freedom to create solutions and make adaptations in a fast and secure application.

### WHO IS TARGETED / OBJECTIVE
Startups and large corporations. Promises speed to create solutions with a high degree of complexity and expansion.

###### For the project
Install apache, php and sql in your (xampp, wamp or individual) mode, install composer, git, node js, npm, bower.

### INSTALLATION
Create a folder with the name of your project and download this content inside, now follow these steps:
1. Run the terminal, or cmd for windows in the folder that placed the project files.
2. Type: `composer install`
3. Type: `bower install`
4. Now, for e-mail to work locally * __ enable the extension open_openssl and php_sockets __ ** in php.ini, just open php.ini and using `Ctrl + F` search for the terms openssl, uncomment and then after repeat the procedures for sockets, be sure to restart apache after the procedure;
5. Create a table with username and password and import the project table.

### SETTINGS
All settings that need to be done are in core / configurations / cnInit.php

### RULES
There are 2 main rules for everything to work fine.
* Respect the rules of existing libraries and the ones you add;
* Respect the flow of project organization. (This second rule sounds pretty cheesy because it requires knowledge about the flow, but it will not be a problem);

A good example of rule enforcement. The default orm library for this project works great on any part of the application as long as it is not placed inside classes or functions. Libraries already have their own functions and classes.

### STRUCTURE
You will come across 4 main folders, after everything has installed: **app**, **core**, **public** and **vendor**. If you have used any MVC framework it is easier to understand.
* In **app**: **controllers**, **models** and **views**;
* In the folder **core: **configurations**, **functions** and **languages**;
* In the folder **public**: the files commonly to the front-end;
* **vendor** with php libraries.

FOLLOWS PART OF THE STRUCTURE
- your-project
    app
         - controllers
         - models
         views
    core
        - configurations
        functions
        - files
    public
         - css
         - sources
         - icons
         - images
         js
         - libs
         uploader
    - **vendor**
    bowerrc
    - **editorconfig**
    - **gitignore**
    - **htaccess**
    bower.json
    - **composer.json**
    - **database.mwb**
    - **database.sql**
    - **index.php**
    - **README.md**

Explanation: **Bowerrc** is responsible for setting the folder that we will install our dependency to **bower.json**. The file **config_config** is a configuration file for the development IDE that dictates tab spacing. If you work with git, you will not want to send the folder of the libraries you manage, so **gitignore** lists the directories to avoid. **htaccess** is special because it was meant to be read in almost every lodge. All php libraries are managed here: ** _ composer.json _ **. The **database** files were included because of the included example, the **Mwb type is for the MySQL Workbech** software. **index.php** runs our application and **README.md** is this file that explains everything.

### CONVENTIONS
Conventions adopted in the work environment for the XFrame project.
* Rule for naming classes and tables and columns in a db: **firstName**
* For the tables must have 's' at the end;
* All `classes` should be represented by a nouns and not a verb: Clients, Contacts, Profile, Settings etc;
* All `methods` should be represented by a verb or verbal phrases in the infinitive: saveStatus, editPage, remove etc;
* Rule for link names: **edit/page/im-a-link**;
* Within the main project folders there are two files, **htaccess** and **index.html**, it blocks access to the folder and subdirectories;
* mIsso_E_Um_Model.php, vIsso_E_Um_View.twig, c_Isso_E_Um_Controller.php. Models start with m and so on;
* It is not possible to group the folder of views in sub-folders, with the exception of subfolders for templates, so the view receives this nomenclature ** __ v + Module name + Module page __ **, such as vTodo_List, vTodex_Edit.
* Configuration files begin with **cn**, **fn** functions and **lg** translations of languages. Example: cnConnection.php, fnValidate.php, lgBR.php
* I realized that everything is in English, it is a good idea to keep this way to leave the universal application from the beginning.

EXAMPLE OF APPLICATION
- controllers
    - api
        - cHome.php
    - Login
        - cActive.php
        - cLogin.php
        - cLogout.php
        - cRecovery.php
        - cRegister.php
    - site
        - cHome.php
- models
    - Login
        - mCheck_User.php
        - mRegister_New_User.php
        - mCheck_Key.php
        - mGet_Id_User_By_Key.php
        - ...
views
    - vApi_Home.twig
    - vSite_Home.twig
    - vLogin_Active.twig
    - vLogin_Login.twig
    - vLogin_New_Pass.twig
    - vLogin_Recovery.twig
    - vLogin_Register.twig

### PROJECT LIBRARIES
* We use [Flight framework] (http://flightphp.com/) as a framework for routes, because it performs better than the Slim Framework or other top of the line;

* MySql database and [Medoo] library (http://medoo.in/). Medoo is by far the best library I've ever met. The rule of another is that their classes can not in any way be within classes and or functions;

* Other libraries: [Twig] (http://twig.sensiolabs.org) to more securely manage and separate views and the classic PHP Mailer for sending e-mails, Docktrine Cache for applications with the accumulation of data in cache, pagseguro etc.

### ROUTES
Think of a simple thing to handle. See official route documentation at: http://flightphp.com/learn/
```
Flight :: route ('/ login', function () {

session_start ();
if (isset ($ _ SESSION ['passport']) OR isset ($ _ COOKIE ['passport'])) {
Flight :: redirect ('/ api'); #### REDIRECT HERE
}
else {

require CONNECTION;
require VALIDATE;
require LANGUAGE;

$ data = array (
'base_url' => BASE_URL,
'alert' => 'Use the lang file to pass variables that do not come from a db',
'link_recovery' => BASE_URL.'recovery '
);
$ query = array_merge ($ data, $ lang);
Flight :: view () -> display ('vLogin_Login.twig', $ query);

}
});
/ * ================================================================ ====================
====================================================================== ===================== * /
Flight :: route ('POST / register / gear', function () {
...
});
/ * ================================================================ ====================
====================================================================== ===================== * /
Flight :: route ('/ register / feedback / @ feedback', function ($ feedback) {

require LANGUAGE;

switch ($ feedback) {
case email
$ fb = $ lang ['lang_fb_email'];
break;
case "wrongpass":
$ fb = $ lang ['lang_fb_wrongpass'];
break;
case "registred":
$ fb = $ lang ['lang_fb_registred'];
break;
default:
$ fb = "";
}

$ data = array (
'base_url' => BASE_URL,
'feedback' => $ fb
);

$ query = array_merge ($ data, $ lang);
Flight :: view () -> display ('vLogin_Register.twig', $ query);
});
```

```
<! DOCTYPE html>
<html>
<body>

<h1> Logar </ h1>

<form action = "{{base_url}} login / gear" method = "post">
  Email: <br>
  <input type = "text" name = "email">
  Home
  Password:
  <input type = "password" name = "pass">
  Home
  <input type = "checkbox" name = "remember" value = "yes" checked = "checked"> Remember Me
  Home
  <input type = "submit" value = "Submit">
  Home
  {{feedback}}
</ form>
<a href="{{ link_recovery }}"> I forgot my password </a>

</ body>
</ html>
```

### DATABASE
Medoo is without a doubt the simplest ORM I have ever known in my life.
You can study the developed login application and consult the official guide at: [medoo.in] (http://medoo.in).

### TEMPLATE SYSTEM
I chose twig for ease of learning, for having used enough with microframeworks as well. If you look at a [summary sheet] (https://s-media-cache-ak0.pinimg.com/originals/9b/7c/f0/9b7cf0ed69f91af8bdbf3d55ec5f893e.jpg) about twig you can get a lot of doubts, however, if you are already accustomed to with Smarty for example you can replace.

----------------------------
#### IF YOU HAVE COME HERE
Thank you very much for your attention.

#### ABOUT THE AUTHOR / ORGANIZER
Philipe Cairon Medeiros de Siqueira
contato@caironm.com
Ideas? Let's discuss. Whats .: 84 93300.0966