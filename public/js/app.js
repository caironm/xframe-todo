/*
// THIS IS A EXAMPLE OF HOW FUNCTION THE AJAX
function name(id) {
    $.post('link', {
        id:id,
        name:document.getElementById('name').value,
        email:document.getElementById('email').value
    }, function(data) {
		if (data == 'success') {
			document.getElementById('form').setAttribute("class", "hidden animated fadeOut");
			setTimeout(function() { $("#form").remove(); }, 800);
			$('#element').text('Vai aparecer esse texto depois');
		} else {
			alert(data);
		}
	});
}*/

function check(id) {
    $.post(document.getElementById('site').value+'update-status',
    {
        id:id,
        status:document.getElementById('status'+id).value
    }, function(data) {
        if (data == 'success') {
            $("#task"+id).attr("class","form-check-label complete");
            $("#check"+id).attr("onclick","uncheck('"+id+"')");
            $("#status"+id).val("1");
        } else {
            alert(data);
        }
	});
}

function uncheck(id) {
    $.post(document.getElementById('site').value+'update-status',
    {
        id:id,
        status:document.getElementById('status'+id).value
    }, function(data) {
        if (data == 'success') {
            $("#task"+id).attr("class","form-check-label");
            $("#check"+id).attr("onclick","check('"+id+"')");
            $("#status"+id).val("0");
        } else {
            alert(data);
        }
	});
}

function edit(id) {
    $('#check'+id).hide('fast');
    $('#task'+id).hide('fast');
    $("#editnow"+id).attr("class","");
    $('#editnow'+id).show('slow');
};

function confirm(id) {
    $.post(document.getElementById('site').value+'update',
    {
        id:id,
        task:document.getElementById('newtask'+id).value,
        status:document.getElementById('status'+id).value
    }, function(data) {
        if (data == 'success') {

            if (status == 0) {
                $("#editnow"+id).hide('fast');
                $("#check"+id).show('fast');
                $("#task"+id).val(+task);
                $("#task"+id).show('fast');
            }
            if (status == 1) {
                $("#editnow"+id).hide('fast');
                $("#check"+id).show('fast');
                $("#task"+id).val(+task);
                $("#task"+id).attr("class","form-check-label complete");
                $("#task"+id).show('fast');
            }
        } else {
            alert(data);
        }
	});
}

function del(id) {
    $.post(document.getElementById('site').value+'delete',
    {
        id:id
    }, function(data) {
        if (data == 'success') {
            $("#mytask"+id).hide('fast');
        } else {
            alert(data);
        }
	});
}

function add() {
    $('#create').show('slow');
};

function create() {
    $.post(document.getElementById('site').value+'register',
    {
        task:document.getElementById('newtask').value
    }, function(data) {
        if (data == 'success') {
            $("#create").hide('slow');
            window.location.reload();
        } else {
            alert(data);
        }
	});
}