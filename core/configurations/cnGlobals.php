<?php
/* --------------------------------------------------------------------------
| Description of model
-------------------------------------------------------------------------- */
define("DELETE_TASK",            	PATH_TO_M."site/mDelete_Task.php");
define("LISTING_TASK",            	PATH_TO_M."site/mListing_Task.php");
define("REGISTER_TASK",            	PATH_TO_M."site/mRegister_Task.php");
define("UPDATE_STATUS_TASK",        PATH_TO_M."site/mUpdate_Status_Task.php");
define("UPDATE_TASK",               PATH_TO_M."site/mUpdate_Task.php");